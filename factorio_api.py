import logging

import requests
from requests.packages.urllib3.util import Retry
from requests.adapters import HTTPAdapter
from box import Box

ENDPOINT = 'http://mods.factorio.com/api/'
MESSAGES_ENDPOINT = ENDPOINT + 'messages'
MESSAGE_ENDPOINT = ENDPOINT + 'messages/%d'
MODS_ENDPOINT = ENDPOINT + 'mods'

session = requests.session()
adapter = HTTPAdapter(max_retries=Retry(status_forcelist=[500, 503]))
session.mount('https://', adapter)
session.mount('http://', adapter)


def box(resp):
    return Box(resp.json(), default_box=True)


def get(url, **kwargs):
    resp = session.get(url, **kwargs)
    logging.debug('GET: %s' % resp.url)
    return resp


def paginate(url, limit=None, **kwargs):
    count = 0
    while True:
        resp = get(url, params=kwargs)
        data = box(resp)

        for result in data.results:
            count += 1
            yield result

            if limit and count >= limit:
                return

        if data.pagination.page < data.pagination.page_count:
            kwargs['page'] = data.pagination.page + 1
        else:
            break


def get_mods(**kwargs):
    return paginate(MODS_ENDPOINT, **kwargs)


def get_messages(**kwargs):
    return paginate(MESSAGES_ENDPOINT, **kwargs)


def get_message(message_id):
    resp = get(MESSAGE_ENDPOINT % message_id)
    if resp.status_code == 404:
        return None
    return box(resp)
