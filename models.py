from os import urandom
from urllib import quote

from flask import url_for
from google.appengine.ext import ndb

MOD_URL = 'https://mods.factorio.com/mods/%s/%s'


class Mod(ndb.Model):
    name = ndb.StringProperty()
    owner = ndb.StringProperty()
    latest_version = ndb.StringProperty()

    @property
    def url(self):
        return MOD_URL % (
            quote(self.owner),
            quote(self.name),
        )


class Mods(ndb.Model):
    """Singleton entity that contains the list of mods and their owners"""

    mods = ndb.StructuredProperty(Mod, repeated=True, indexed=False)

    # The latest mod update time in the Mod Portal's time, not ours
    last_update = ndb.DateTimeProperty(indexed=False)

    # The time of the last successful mods database check in local time
    last_check = ndb.DateTimeProperty(auto_now=True, indexed=False)


class Messages(ndb.Model):
    """Singleton entity that tracks the date of the last known message."""

    # The latest message update time is in the Mod Portal's time, not ours
    last_update = ndb.DateTimeProperty(indexed=False)

    # The time of the last successful messages check in local time
    last_check = ndb.DateTimeProperty(auto_now=True, indexed=False)


class User(ndb.Model):
    confirmed = ndb.BooleanProperty()
    auth_key = ndb.StringProperty()

    username = ndb.StringProperty()
    msg_notif_own_mods = ndb.BooleanProperty(default=True)
    msg_notif_own_threads = ndb.BooleanProperty(default=True)
    msg_include_self = ndb.BooleanProperty(default=False)

    mods = ndb.StringProperty(repeated=True)
    mod_notif_watched = ndb.BooleanProperty(default=True)
    msg_notif_watched = ndb.BooleanProperty(default=False)

    @property
    def email(self):
        return self.key.id()

    def create_key(self):
        # TODO: rate-limit
        self.auth_key = urandom(16).encode('hex')

    @classmethod
    def register(cls, email):
        user = User(id=email)
        user.create_key()
        user.put()
        return user

    @classmethod
    def auth_or_confirm(cls, email, key):
        user = cls.get_by_id(email)
        confirmed = user and user.confirmed

        if user and key == user.auth_key:
            if not confirmed:
                user.confirmed = True
                user.put()
            return user, confirmed

        return None, False

    @property
    def auth_url(self):
        if not self.auth_key:
            raise ValueError("No key found")

        return url_for(
            'index',
            email=self.email,
            key=self.auth_key,
            _external=True,
        )
