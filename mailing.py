import time
import email.utils

import CommonMark
import logging
from mailjet_rest import Client
from flask import current_app, escape, render_template


class NonMoronicHtmlRenderer(CommonMark.HtmlRenderer):
    '''HTML renderer that escapes inline HTML rather than just stripping it'''

    def html_inline(self, node, entering):
        self.lit(escape(node.literal))

    def html_block(self, node, entering):
        self.lit(escape(node.literal))
        self.cr()


def render_mail(template, **kwargs):
    message_plain = render_template(
        template,
        **kwargs
    )

    parser = CommonMark.Parser()
    ast = parser.parse(message_plain)
    renderer = NonMoronicHtmlRenderer(options=dict(safe=True))
    message_html = renderer.render(ast)

    return message_plain, message_html


def send_mail(to, subject, message_html, message_plain,
              sender_name=None, date=None):
    if not sender_name:
        sender_name = current_app.config['APPLICATION_NAME']

    if date:
        date = email.utils.formatdate(time.mktime(date.timetuple()))

    sender_email = current_app.config['MAILJET_SENDER']
    mailjet = Client(
        auth=(current_app.config['MAILJET_API_KEY'],
              current_app.config['MAILJET_API_SECRET'])
    )

    mail = {
        'FromName': sender_name,
        'FromEmail': sender_email,
        'To': to,
        'Subject': subject,
        'Text-part': message_plain,
        'Html-part': message_html,
        'Headers': {}
    }

    if date:
        mail['Headers']['Date'] = date

    resp = mailjet.send.create(mail)

    if resp.status_code != 200:
        logging.error("Could not send mail: %r: %r", resp, mail)

    return resp
