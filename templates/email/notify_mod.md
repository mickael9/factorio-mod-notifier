{% extends "email/base.md" %}

{% block message %}
Version **{{mod.latest_version}}** of the mod *{{ mod.name|mdescape }}* has been released.
{% endblock %}

{% block footer %}
[View this mod on the Mod Portal]({{ mod.url }})

*To change your settings or unsubscribe, please use [this link]({{ user.auth_url }}).*

{{ super() }}
{% endblock %}
