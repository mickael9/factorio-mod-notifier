{% extends "email/base.md" %}

{% block message %}
You have requested to register to the *{{ config.APPLICATION_NAME }}* service.

{% if user.confirmed %}
Your email is already registered. Use the following link to change your settings:
{% else %}
To finish your registration, please click the following link:
{% endif %}

<{{ user.auth_url }}>

If you wish to change your settings in the future, you can reuse that same link (bookmark it).

If you didn't make such a request, please ignore this message.

{% endblock %}
