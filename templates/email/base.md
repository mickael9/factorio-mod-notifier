{% autoescape false %}
{% block message %}{% endblock %}
---
{% block footer %}
*[{{ request.host }}]({{ request.host_url }}) ⋅ {{ config.DISCLAIMER }}*
{% endblock %}
{% endautoescape %}
