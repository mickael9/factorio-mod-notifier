{% extends "email/base.md" %}

{% block message %}
{{ message.message }}
{% endblock %}

{% block footer %}
[View this thread on the Mod Portal]({{ message.thread_url }})

*To change your settings or unsubscribe, please use [this link]({{ user.auth_url }}).*

{{ super() }}
{% endblock %}
