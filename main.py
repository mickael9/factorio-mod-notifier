import re
import logging
from os import urandom

import requests
import requests_toolbelt.adapters.appengine
from google.appengine.ext import ndb
from google.appengine.api import urlfetch
from flask import (
    Flask, abort, render_template, request, session,
    flash, url_for, redirect, jsonify, Markup
)
from flask_bootstrap import Bootstrap

import config
from models import User, Mods
from mailing import render_mail, send_mail

RECAPTCHA_ENDPOINT = 'https://www.google.com/recaptcha/api/siteverify'

# https://html.spec.whatwg.org/#valid-e-mail-address
EMAIL_RE = re.compile(
    r"[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@"
    r"[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?"
    r"(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*")

app = Flask(__name__)
app.config.from_object(config)
Bootstrap(app)

requests_toolbelt.adapters.appengine.monkeypatch()

import cron  # noqa


@app.before_request
def before_request():
    # The Factorio API can be so painfully slow...
    urlfetch.set_default_fetch_deadline(60)

    if (request.host != app.config['APPLICATION_HOST']
            and 'X-AppEngine-Cron' not in request.headers
            and not request.environ['SERVER_SOFTWARE'].startswith(
                'Development'
            )
            and not request.host.endswith(
                '-dot-factorio-mod-notifier.appspot.com'
            )):

        abort(redirect("https://%s%s" % (
            app.config['APPLICATION_HOST'],
            request.full_path
        )))

    # http://flask.pocoo.org/snippets/3/
    if request.method == 'POST':
        token = session.pop('_csrf_token', None)
        if not token or token != request.form.get('_csrf_token'):
            flash('Invalid form', 'error')
            abort(redirect(url_for('index')))


@app.template_global()
def csrf():
    if '_csrf_token' not in session:
        session['_csrf_token'] = urandom(16).encode('hex')

    return Markup(
        '<input name="_csrf_token" type="hidden" value="%s">'
    ) % session['_csrf_token']


@app.template_filter()
def mdescape(s):
    return re.sub(r'[^A-Za-z0-9-]', r'\\\0', s)


def check_recaptcha():
    response = request.form.get('g-recaptcha-response')

    if not response:
        return False

    res = requests.post(RECAPTCHA_ENDPOINT, data=dict(
        secret=app.config['RECAPTCHA_SECRET_KEY'],
        response=response,
        remoteip=request.remote_addr
    ))

    if res.status_code != 200:
        return False

    return res.json().get('success')


def get_user():
    email = request.args.get('email', '')
    key = request.args.get('key', '')
    user = None

    # Check for GET-provided authentication credentials
    if email or key:
        if email and key:
            user, was_confirmed = User.auth_or_confirm(email, key)
        if user:
            session.clear()
            session['email'] = email
            session['key'] = key

            if user and not was_confirmed:
                flash("Your account was activated.", 'success')
        else:
            flash("The link you followed was invalid.", 'error')

        abort(redirect(url_for('index')))

    # Else use session credentials
    email = session.get('email', '')
    key = session.get('key', '')

    if email and key:
        user, _ = User.auth_or_confirm(email, key)
        if not user:
            session.clear()
        return user


def register():
    email = request.form.get('email', '').strip()

    if not (request.method == 'POST' and email):
        return

    if not EMAIL_RE.match(email):
        flash("Invalid email address", 'error')
        return

    if not check_recaptcha():
        flash("Recaptcha verification failed.", 'error')
        return

    user = User.get_by_id(email)
    if not user:
        user = User.register(email)

    if not user:
        flash("Unknown error.", 'error')
        return

    plain, html = render_mail(
        'email/confirm.md',
        user=user,
    )

    if not send_mail(user.email, app.config['APPLICATION_NAME'], html, plain):
        flash("An error has occured.", 'error')
        return

    flash("A confirmation email was sent to {}.".format(user.email),
          'success')

    return redirect(url_for('index'))


def settings(user):
    if request.method != 'POST':
        return

    if request.form.get('unsub'):
        user.key.delete()
        session.clear()
        flash("Your account was deleted.", 'success')
    elif request.form.get('regen'):
        user.create_key()
        user.put()
        session['key'] = user.auth_key
        flash("Your account key was regenerated.", 'success')
    else:
        user.username = request.form.get('username', '')
        user.mods = [
            m.strip()
            for m in request.form.get('mods', '').split(',')
            if m.strip()
        ]
        for prop in (
                'msg_notif_own_mods', 'msg_notif_own_threads',
                'msg_include_self', 'mod_notif_watched',
                'msg_notif_watched'):
            setattr(user, prop, bool(request.form.get(prop)))

        user.put()
        flash("Settings updated successfully.", 'success')

    return redirect(url_for('index'))


@app.route('/', methods=['GET', 'POST'])
@ndb.transactional
def index():
    user = get_user()

    ret = (settings(user) if user else
           register())

    if ret:
        return ret

    if user:
        return render_template('settings.html', user=user)
    else:
        return render_template('register.html')


@app.route('/logout', methods=['POST'])
def logout():
    session.clear()
    flash(
        "Logged out. You can log in again using the link emailed to you. "
        "If you loose the link, just fill the subscription form again.",

        'success'
    )
    return redirect(url_for('index'))


@app.route('/json/authors')
def authors():
    mods = Mods.get_by_id('mods')
    query = request.args.get('q', '').lower()
    authors = sorted(set(
        m.owner
        for m in mods.mods
        if m.owner.lower().startswith(query)
    ), key=unicode.lower)
    return jsonify([dict(value=name) for name in authors])


@app.route('/json/mods')
def mods():
    mods = Mods.get_by_id('mods')
    query = request.args.get('q', '').lower()

    if len(query) < 1:
        return jsonify([])

    authors = sorted(set(
        m.name
        for m in mods.mods
        if query in m.name.lower()
    ), key=lambda val: (not val.lower().startswith(query), val))

    return jsonify([dict(value=name) for name in authors])


@app.errorhandler(500)
def server_error(e):
    logging.exception("An error occurred during a request.")
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500
