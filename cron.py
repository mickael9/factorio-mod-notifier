import logging
from urllib import quote
from collections import defaultdict

import dateutil.parser
from google.appengine.ext import ndb
from flask import abort, request

import factorio_api
from main import app
from mailing import render_mail, send_mail
from models import Mod, Mods, User, Messages

THREAD_URL = 'https://mods.factorio.com/mods/%s/%s/discussion/%d'


def parse_date(datestring):
    return dateutil.parser.parse(datestring, ignoretz=True)


def send_message_notification(user, message):
    subject = "%s[%s][%s] %s" % (
            "Re: " if message.parent else '',
            message.mod,
            message.tag.upper(),
            message.title
    )
    to = user.email
    sender = message.user
    date = message.created_at

    message_plain, message_html = render_mail(
        'email/notify_message.md',
        message=message,
        user=user,
    )

    send_mail(
        to, subject, message_html, message_plain,
        sender_name=sender, date=date
    )


def send_message_notifications(mod_name, mod_author, thread_author, messages):
    logging.debug(
        "Sending message notifications (%d messages) for mod: %s",
        len(messages),
        mod_name
    )
    total_sent = 0

    q = User.query(User.confirmed == True) # noqa

    or_clauses = [ndb.AND(
        User.msg_notif_watched == True,
        User.mods == mod_name
    )] # noqa

    if thread_author:
        or_clauses.append(ndb.AND(
            User.msg_notif_own_threads == True,
            User.username == thread_author
        )) # noqa

    if mod_author:
        or_clauses.append(ndb.AND(
            User.msg_notif_own_mods == True,
            User.username == mod_author
        )) # noqa

    q = q.filter(ndb.OR(*or_clauses))

    for user in q.iter():
        nb_sent = 0
        for msg in messages:
            if user.msg_include_self or msg.user != user.username:
                send_message_notification(user, msg)
                nb_sent += 1
        if nb_sent:
            logging.debug("Sent %d email(s) to %s", nb_sent, user.email)
            total_sent += nb_sent

    return total_sent


def send_mod_notification(user, mod):
    subject = "New release of %s" % mod.name
    to = user.email

    message_plain, message_html = render_mail(
        'email/notify_mod.md',
        user=user,
        mod=mod,
    )

    send_mail(to, subject, message_html, message_plain)


def send_mod_notifications(mod):
    logging.debug("Sending update notifications for mod: %s", mod.name)
    sent = 0
    users = User.query(
        User.confirmed == True,
        User.mod_notif_watched == True,
        User.mods == mod.name
    ) # noqa

    for user in users.iter():
        send_mod_notification(user, mod)
        sent += 1

    return sent


def cron_middleware(wsgi_app):
    def wrap(environ, start_response):
        import config

        if environ.get('HTTP_X_APPENGINE_CRON'):
            environ['SERVER_NAME'] = config.APPLICATION_HOST
            environ['HTTP_HOST'] = config.APPLICATION_HOST
            environ['HTTPS'] = 'on'
            environ['wsgi.url_scheme'] = 'https'

        return wsgi_app(environ, start_response)
    return wrap


app.wsgi_app = cron_middleware(app.wsgi_app)


@app.route('/cron')
def mods_cron():
    if not request.headers.get('X-Appengine-Cron'):
        abort(404)

    db_mods = update_mods()
    update_messages(db_mods)

    return 'OK'


def update_mods():
    logging.info("Updating mods...")

    db_mods = Mods.get_or_insert('mods')
    db_last_update = db_mods.last_update
    page_size = 25 if db_last_update else 'max'
    api_mods = factorio_api.get_mods(page_size=page_size, order='updated')
    added = 0
    updated = []

    for api_mod in api_mods:
        api_mod.updated_at = parse_date(api_mod.updated_at)
        db_mods.last_update = max(
            db_mods.last_update or api_mod.updated_at,
            api_mod.updated_at
        )

        logging.info(
            "Mod %s, updated=%s",
            api_mod.name, api_mod.updated_at,
        )

        if db_last_update and api_mod.updated_at <= db_last_update:
            break

        try:
            mod = next(m for m in db_mods.mods if m.name == api_mod.name)
            updated.append(mod)
        except StopIteration:
            mod = Mod(name=api_mod.name)
            db_mods.mods.append(mod)
            logging.debug("New mod: %s", mod.name)
            added += 1

        mod.owner = api_mod.owner
        mod.latest_version = api_mod.latest_release.version or ''

    db_mods.put()

    total_sent = 0
    for mod in updated:
        try:
            total_sent += send_mod_notifications(mod)
        except Exception as ex:
            logging.exception(ex)

    logging.info(
        "%d mods in db (%d added, %d updated). Sent %d email(s)." % (
            len(db_mods.mods),
            added,
            len(updated),
            total_sent
        )
    )
    return db_mods


def update_messages(db_mods):
    logging.info("Updating messages...")
    db_messages = Messages.get_or_insert('messages')
    db_last_update = db_messages.last_update

    root_messages = {}
    updated_threads = defaultdict(list)

    messages = factorio_api.get_messages(page_size=25)

    for message in messages:
        message.last_reply_at = parse_date(message.last_reply_at)
        message.created_at = parse_date(message.created_at)
        message.updated_at = parse_date(message.updated_at)
        db_messages.last_update = max(
            db_messages.last_update or message.last_reply_at,
            message.last_reply_at
        )
        logging.info(
            "Message %d, last_reply=%s, created=%s",
            message.id, message.last_reply_at, message.created_at
        )

        if db_last_update is None:
            # Don't spam everyone on init!
            break

        if message.last_reply_at <= db_last_update:
            break

        if not message.parent:
            root_messages[message.id] = message
            thread = message
        else:
            thread = root_messages.get(message.parent)
            if not thread:
                thread = factorio_api.get_message(message.parent)
            if not thread:
                # Possible if the thread was deleted
                # It seems child messages are not removed with their parent
                continue
            message.mod = message.mod or thread.mod
            message.title = message.title or thread.title

        mod_author = ''
        try:
            mod_author = next(mod.owner
                              for mod in db_mods.mods
                              if mod.name == message.mod)
        except StopIteration:
            logging.error("Unknown author for mod: %s" % message.mod)

        message.mod_author = mod_author

        message.thread_url = THREAD_URL % (
            quote(message.mod_author),
            quote(message.mod),
            thread.id
        )

        if message.created_at > db_last_update:
            updated_threads[thread.id].append(message)

        if message.created_at >= message.last_reply_at or \
                message.children_count == 0:
            continue

    db_messages.put()

    total_sent = 0
    for id, updated_thread in updated_threads.items():
        if not updated_thread:
            continue

        try:
            messages = sorted(updated_thread,
                              key=lambda e: e.created_at)

            total_sent += send_message_notifications(
                mod_name=updated_thread[0].mod,
                mod_author=updated_thread[0].mod_author,
                thread_author=updated_thread[0].user,
                messages=messages
            )
        except Exception as e:
            logging.exception(e)

    logging.info(
        "Updated %d message(s) in %d thread(s). Sent %d email(s)." % (
            sum(len(x) for x in updated_threads.values()),
            len(updated_threads),
            total_sent,
        )
    )
